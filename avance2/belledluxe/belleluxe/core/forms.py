from django import forms
from .models import *

#campaign----------------------------------------------------------------------------

class NewCampaignform(forms.ModelForm):
   class Meta:
       model = campaign
       fields = [
           "lasteditor",
           "name"
       ]
       widgets = {
            "name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del Producto"}),
            "lasteditor": forms.Select(attrs={"class":"form-select form-control"}),
        }
       labels = {
        "name": "Nombre",
        "lasteditor": "Ultima persona que lo edito"
        }


class UpdateCampaignForm(forms.ModelForm):
   class Meta:
       model = campaign
       fields = [
           "lasteditor",
           "name"
       ]
       widgets = {
            "name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del Producto"}),
            "lasteditor": forms.Select(attrs={"class":"form-select form-control"}),
        }
       labels = {
        "name": "Nombre",
        "lasteditor": "Ultima persona que lo edito"
        }


#gender----------------------------------------------------------------------------
class NewGenderForm(forms.ModelForm):
   class Meta:
       model = gender
       fields = [
           "lasteditor",
           "name",
       ]
       widgets = {
            "name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del Producto"}),
            "lasteditor": forms.Select(attrs={"class":"form-select form-control"}),
        }
       labels = {
        "name": "Nombre",
        "lasteditor": "Ultima persona que lo edito"
        }

class UpdateGenderForm(forms.ModelForm):
   class Meta:
       model = gender
       fields = [
           "lasteditor",
           "name",
       ]
       widgets = {
            "name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del Producto"}),
            "lasteditor": forms.Select(attrs={"class":"form-select form-control"}),
        }
       labels = {
        "name": "Nombre",
        "lasteditor": "Ultima persona que lo edito"
        }
#category----------------------------------------------------------------------------
class NewCategoryForm(forms.ModelForm):
   class Meta:
       model = category
       fields = [
           "lasteditor",
           "name",
       ]
       widgets = {
            "name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del Producto"}),
           
            "lasteditor": forms.Select(attrs={"class":"form-select form-control"}),
        }
       labels = {
        "name": "Nombre",
        "lasteditor": "Ultima persona que lo edito"
        }

class UpdateCategoryForm(forms.ModelForm):
   class Meta:
       model = category
       fields = [
           "lasteditor",
           "name",
       ]
       widgets = {
            "name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del Producto"}),
            "lasteditor": forms.Select(attrs={"class":"form-select form-control"}),
        }
       labels = {
        "name": "Nombre",
        "lasteditor": "Ultima persona que lo edito"
        }
#payment----------------------------------------------------------------------------
class NewPaymentForm(forms.ModelForm):
   class Meta:
       model = payment
       fields = [
           "name",
           "reference",
       ]
       widgets = {
            "name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del Producto"}),
            "reference": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del Producto"}),
        }
       labels = {
        "name": "Nombre",
        "reference": "Referencia"
        }
       

class UpdatePaymentForm(forms.ModelForm):
   class Meta:
       model = payment
       fields = [
           "name",
           "reference",
       ]
       widgets = {
            "name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del Producto"}),
            "reference": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del Producto"}),
        }
       labels = {
        "name": "Nombre",
        "reference": "Referencia"
        }
       
#product----------------------------------------------------------------------------
class NewProductForm(forms.ModelForm):
    class Meta:
        model = product
        fields = [
            "lasteditor",
            "name",
            "imagen1",
            "imagen2",
            "imagen3",
            "campaign",
            "gender",
            "category",
            "description",
            "size",
            "price_uni",
            "stock",
            "status",
            
        ] 

        widgets = {
            "lasteditor": forms.Select(attrs={"class":"form-select form-control"}),
            "name": forms.TextInput(attrs={ "type":"text", "class":"form-control", "placeholder":"Escribe el nombre del Producto"}),
            "imagen1": forms.FileInput(attrs={ "type":"file", "class":"form-control"}),
            "imagen2": forms.FileInput(attrs={ "type":"file", "class":"form-control", }),
            "imagen3": forms.FileInput(attrs={"type":"file", "class":"form-control"}),
            "campaign": forms.Select(attrs={"class":"form-select form-control"}),
            "gender": forms.Select(attrs={"class":"form-select form-control"}),
            "category": forms.Select(attrs={"class":"form-select form-control"}),
            "description": forms.Textarea(attrs={"type":"text", "class":"form-control", "rows": 4, "placeholder":"Escribe la descripcion del Producto"}),
            "size": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe la talla del Producto"}),
            "price_uni" : forms.TextInput(attrs={"type":"number", "class":"form-control", "step":"any"}),
            "stock": forms.TextInput(attrs={"type":"number", "class":"form-control"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox"}),
            
        }
        labels = {
        "lasteditor" : "Ultima persona que lo edito",
        "name": "Nombre",
        "imagen1": "Imagen 1",
        "imagen2": "Imagen 2",
        "imagen3": "Imagen 3",
        "campaign": "Campaña",
        "gender": "Genero",
        "category": "Categoria",
        "description": "Descripción",
        "size":"Talla",
        "price_uni":"Precio unitario",
        "stock":"Cantidad",
        "status":"Status",
        
    }



class UpdateProductForm(forms.ModelForm):
    class Meta:
        model = product
        fields = [
            "lasteditor",
            "name",
            "imagen1",
            "imagen2",
            "imagen3",
            "campaign",
            "gender",
            "category",
            "description",
            "size",
            "price_uni",
            "stock",
            "status",
            
        ] 

        widgets = {
            "lasteditor": forms.Select(attrs={"class":"form-select form-control"}),
            "name": forms.TextInput(attrs={ "type":"text", "class":"form-control", "placeholder":"Escribe el nombre del Producto"}),
            "imagen1": forms.FileInput(attrs={ "type":"file", "class":"form-control"}),
            "imagen2": forms.FileInput(attrs={ "type":"file", "class":"form-control", }),
            "imagen3": forms.FileInput(attrs={"type":"file", "class":"form-control"}),
            "campaign": forms.Select(attrs={"class":"form-select form-control"}),
            "gender": forms.Select(attrs={"class":"form-select form-control"}),
            "category": forms.Select(attrs={"class":"form-select form-control"}),
            "description": forms.Textarea(attrs={"type":"text", "class":"form-control", "rows": 4, "placeholder":"Escribe la descripcion del Producto"}),
            "size": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe la talla del Producto"}),
            "price_uni" : forms.TextInput(attrs={"type":"number", "class":"form-control", "step":"any"}),
            "stock": forms.TextInput(attrs={"type":"number", "class":"form-control"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox"}),
            
        }
        labels = {
        "lasteditor" : "Ultima persona que lo edito",
        "name": "Nombre",
        "imagen1": "Imagen 1",
        "imagen2": "Imagen 2",
        "imagen3": "Imagen 3",
        "campaign": "Campaña",
        "gender": "Genero",
        "category": "Categoria",
        "description": "Descripción",
        "size":"Talla",
        "price_uni":"Precio unitario",
        "stock":"Cantidad",
        "status":"Status",
        
    }
        
#Customer-------------------------------------------------------------------------
"""
class NewCustomerForm (forms.ModelForm): 
    class Meta: 
        model= customer
        fields = [ 
            "user",
            "name_first",
            "last_name_pat",
            "last_name_mat",
            "phone",
            "cp",
            "streat",
            "number_home",
        ]
        widgets = {
            "user": forms.Select(attrs={"class":"form-select form-control"}),
            "name_first":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Nommbre del cliente"}) ,
            "last_name_pat": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Apellido paterno del cliente"}),
            "last_name_mat": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Apellido materno del cliente"}),
            "phone":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Numero de telefono"}),
            "cp":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Codigo postal"}),
            "streat":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Direccion del cliente"}),
            "number_home":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"NUmero de casa"})
            }
        
    class UpdateCustomerForm (forms.ModelForm):
        class Meta:
            model = order
            fields = [ 
            "user",
            "name_first",
            "last_name_pat",
            "last_name_mat",
            "phone",
            "cp",
            "streat",
            "number_home",
        ]
        widgets = {
            "user": forms.Select(attrs={"class":"form-select form-control"}),
            "name_first":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Nombre del cliente"}) ,
            "last_name_pat": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Apellido paterno del cliente"}),
            "last_name_mat": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Apellido materno del cliente"}),
            "phone":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Numero de telefono"}),
            "cp":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Codigo postal"}),
            "streat":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Direccion del cliente"}),
            "number_home":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"NUmero de casa"})
            }


#Order----------------------------------------------------------------------------

class NewOrderForm ( forms.ModelForm): 
    class Meta: 
        model= order
        fields = [ 
            "payment",
            "customer",
            "Date",
            "subtotal",
            "IVA",
            "total",
        ]
        widgets = {
            "payment":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Numero del pago"}),
            "customer" : forms.TextInput (sttrs={"type": "text", "class":"form-control", "placeholder":"Nombre del cliente" }),
            "date": forms.DateField (sttrs={"type": "text", "class":"form-control", "placeholder":"Fecha" }),
            "code_flow": forms.TextInput (attrs={"type":"text", "class":"form-control", "placeholder":"codigo de seguimiento"}),
            "subtotal": forms.TextInput (attrs={"type":"text", "class":"form-control", "placeholder":"subtotal"}),
            "IVA": forms.TextInput (attrs={"type":"text", "class":"form-control", "placeholder":"IVA"}),
            "total": forms.TextInput (attrs={"type":"text", "class":"form-control", "placeholder": "Total"}),
        }

    class UpdateOrderForm (forms.ModelForm):
        class Meta:
            model = order
            fields = [ 
            "payment",
            "customer",
            "Date",
            "subtotal",
            "IVA",
            "total",
        ]
        widgets = {
            "payment":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Nombre de Producto"}),
            "customer" : forms.TextInput (sttrs={"type": "text", "class":"form-control", "placeholder":"Nombre del cliente" }),
            "date": forms.DateField (sttrs={"type": "text", "class":"form-control", "placeholder":"Fecha" }),
            "code_flow": forms.TextInput (attrs={"type":"text", "class":"form-control", "placeholder":"Codigo de seguimiento"}) ,
            "subtotal": forms.TextInput (attrs={"type":"text", "class":"form-control", "placeholder":"Subtotal"}),
            "IVA": forms.TextInput (attrs={"type":"text", "class":"form-control", "placeholder":"IVA"}),
            "total": forms.TextInput (attrs={"type":"text", "class":"form-control", "placeholder":"Total"}),
        }
"""