from django.contrib import admin

from home import models

@admin.register(models.Profile)
class Profileadmin(admin.ModelAdmin):
    list_display=[
        "name", 
        "user", 
        "timestamp",
        "age",
        "status"
    ]

@admin.register(models.Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display=["name","user","id"]

@admin.register(models.Product)
class ProductAdmin(admin.ModelAdmin):
    list_display=["product_name","status","category","id"]

