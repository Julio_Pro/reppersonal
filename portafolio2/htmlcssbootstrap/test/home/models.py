from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=16, null=False, blank=False)
    bio = models.TextField(max_length=256, default='Amo esta APP')
    timestamp = models.DateField(auto_now_add=True)
    age = models.IntegerField(default=18)
    weight = models.FloatField(default=50.45)
    status = models.BooleanField(default=True)
    slug = models.SlugField(max_length=16)

    def __str__(self):
        return self.name
    

class Category(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=32, default="Categoria generica")
    status = models.BooleanField(default=True)
    def __str__(self):
        return self.name
    
class Product(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    product_name = models.CharField(max_length=32, default="Producto generico")
    status = models.BooleanField(default=True)
    
    def __str__(self):
        return self.product_name
