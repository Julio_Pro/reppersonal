"""
URL configuration for practica1 project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from home import views
app_name="home"

urlpatterns = [
    

    path('detail/category/<int:pk>/', views.DetailCategory.as_view(), name='detail_category'),
    path('update/category/<int:pk>/', views.UpdateCategory.as_view(), name='update_category'),
    path('delete/category/<int:pk>/', views.DeleteCategory.as_view(), name='delete_category'),
    path("new/category/", views.NewCategory.as_view(), name="new_category"),
    path('', views.Index.as_view(), name="index"),
]
