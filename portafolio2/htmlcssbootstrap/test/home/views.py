from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy

from .models import Category
from .forms import *

# Clases Originales
class Index(generic.View):
    tamplate_name="home/index.html"
    context={}

    def get(self,request):
        self.context= {
            "categories": Category.objects.all()
        }
        return render(request, self.tamplate_name, self.context)

# Crud Category
class DetailCategory(generic.DetailView):
    template_name = "home/detail_category.html"
    model = Category
    

class UpdateCategory(generic.UpdateView):
    template_name="home/update_category.html"
    model = Category
    form_class = UpdateCategoryForm
    success_url = reverse_lazy("home:index")

class DeleteCategory(generic.DeleteView):
    template_name="home/delete_category.html"
    model = Category
    success_url = reverse_lazy("home:index")

class NewCategory(generic.CreateView):
    template_name="home/new_category.html"
    model = Category
    form_class = NewCategoryform
    success_url = reverse_lazy("home:index")


