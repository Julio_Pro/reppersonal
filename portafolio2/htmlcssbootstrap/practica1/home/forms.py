from django import forms 
from .models import *

class UpdateCategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = [
            "user",
            "name",
            "status"
        ]

class NewCategoryform(forms.ModelForm):
    class Meta:
        model = Category
        fields = [
            "user",
            "name",
            "status"
        ]

class UpdateAreaForm(forms.ModelForm):
    class Meta:
        model = Area
        fields = [
            "name",
            "description"
        ]

class NewAreaform(forms.ModelForm):
    class Meta:
        model = Area
        fields = [
            "name",
            "description"
        ]





