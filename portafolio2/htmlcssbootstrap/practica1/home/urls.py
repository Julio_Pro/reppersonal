"""
URL configuration for practica1 project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from home import views
app_name="home"

urlpatterns = [
    

    path('detail/category/<int:pk>/', views.DetailCategory.as_view(), name='detail_category'),
    path('update/category/<int:pk>/', views.UpdateCategory.as_view(), name='update_category'),
    path('delete/category/<int:pk>/', views.DeleteCategory.as_view(), name='delete_category'),
    path("new/category/", views.NewCategory.as_view(), name="new_category"),


    path('cruds/', views.Crud.as_view(), name='crud_tarea'),


    path('cruds/areas/', views.Areas.as_view(), name="areas"),
    path('detail/areas/<int:pk>/', views.DetailAreas.as_view(), name='detail_areas'),
    path('update/areas/<int:pk>/', views.UpdateAreas.as_view(), name='update_areas'),
    path('delete/areas/<int:pk>/', views.DeleteAreas.as_view(), name='delete_areas'),
    path('new/areas/', views.NewArea.as_view(), name='NewArea'),



    path('pagina4/', views.Pagina4.as_view(), name="pagina4"),
    path('pagina5/', views.Pagina5.as_view(), name="pagina5"),
    path('pagina6/', views.Pagina6.as_view(), name="pagina6"),
    path('pagina7/', views.Pagina7.as_view(), name="pagina7"),
    path('pagina8/', views.Pagina8.as_view(), name="pagina8"),
    path('pagina9/', views.Pagina9.as_view(), name="pagina9"),
    path('pagina10/', views.Pagina10.as_view(), name="pagina10"),
    path('pagina11/', views.Pagina11.as_view(), name="pagina11"),
    path('pagina12/', views.Pagina12.as_view(), name="pagina12"),
    path('pagina13/', views.Pagina13.as_view(), name="pagina13"),
    path('pagina14/', views.Pagina14.as_view(), name="pagina14"),
    path('pagina15/', views.Pagina15.as_view(), name="pagina15"),
    path('pagina16/', views.Pagina16.as_view(), name="pagina16"),
    path('pagina17/', views.Pagina17.as_view(), name="pagina17"),
    path('pagina18/', views.Pagina18.as_view(), name="pagina18"),
    path('pagina19/', views.Pagina19.as_view(), name="pagina19"),
    path('pagina20/', views.Pagina20.as_view(), name="pagina20"),

    path('', views.Index.as_view(), name="index"),
]
