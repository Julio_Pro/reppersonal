from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=16, null=False, blank=False)
    bio = models.TextField(max_length=256, default='Amo esta APP')
    timestamp = models.DateField(auto_now_add=True)
    age = models.IntegerField(default=18)
    weight = models.FloatField(default=50.45)
    status = models.BooleanField(default=True)
    slug = models.SlugField(max_length=16)

    def __str__(self):
        return self.name
    

class Category(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=32, default="Categoria generica")
    status = models.BooleanField(default=True)
    def __str__(self):
        return self.name
    
class Product(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    product_name = models.CharField(max_length=32, default="Producto generico")
    status = models.BooleanField(default=True)
    
    def __str__(self):
        return self.product_name

#Tarea-----------------------------------------------------------------------------------------

class BankAccunt(models.Model):
    numbers_accunt  = models.IntegerField(default=00000000000000)
    name_bank = models.CharField(max_length=10, default="coppel")

    def __str__(self):
        return self.name_bank
    
class Worker(models.Model):
    bankaccunt = models.ForeignKey(BankAccunt, on_delete=models.CASCADE)
    name = models.CharField(max_length=10, default="npc")
    status = models.BooleanField(default=True)
    
    def __str__(self):
        return self.name

class Area(models.Model):
    name = models.CharField(max_length=10, default="npc")
    description = models.CharField(max_length=30, default="Descripcion generica")
        
    def __str__(self):
        return self.name

class Boss(models.Model):
    area = models.ForeignKey(Area, on_delete=models.CASCADE)
    name = models.CharField(max_length=20, default="boss_npc")
    last_name = models.CharField(max_length=20, default="Pancho_npc")

    def __str__(self):
        return self.name
    
class Creator(models.Model):
    name = models.CharField(max_length=10, default="npc")
    status = models.BooleanField(default=False)

    def __str__(self):
        return self.name

class Object(models.Model):
    creator = models.ForeignKey(Creator, on_delete=models.CASCADE)
    name = models.CharField(max_length=10, default="Objeto generico")
    description = models.CharField(max_length=30, default="Descripcion generica")

    def __str__(self):
        return self.name
    
class Factory(models.Model):
    name = models.CharField(max_length=10, default="Fabrica generica")
    address = models.CharField(max_length=32, default="Direccion generica")
    state = models.CharField(max_length=15, default="Estado generico")
    def __str__(self):
        return self.name

class Parts(models.Model):
    factory = models.ForeignKey(Factory, on_delete=models.CASCADE)
    name_part = models.CharField(max_length=30, default="piesa generica")
    disponibility = models.BooleanField(default=False)

    def __str__(self):
        return self.name_part

class Car(models.Model):
    car_registration = models.CharField(max_length=5, default="EDQ12")
    name_car = models.CharField(max_length=15, default="tsuru")
    

    def __str__(self):
        return self.name_car
    
class TAXI_DRIVER(models.Model):
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    name = models.CharField(max_length=32, default="el taxista")
    size = models.FloatField(default=1.7)

    def __str__(self):
        return self.name


    
