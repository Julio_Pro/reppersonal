from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy

from .models import Category 

from .forms import *
from .models import * 

# Clases Originales
class Index(generic.View):
    tamplate_name="home/index.html"
    context={}

    def get(self,request):
        self.context= {
            "categories": Category.objects.all()
        }
        return render(request, self.tamplate_name, self.context)

# Crud Category
class DetailCategory(generic.DetailView):
    template_name = "home/detail_category.html"
    model = Category
    

class UpdateCategory(generic.UpdateView):
    template_name="home/update_category.html"
    model = Category
    form_class = UpdateCategoryForm
    success_url = reverse_lazy("home:index")

class DeleteCategory(generic.DeleteView):
    template_name="home/delete_category.html"
    model = Category
    success_url = reverse_lazy("home:index")

class NewCategory(generic.CreateView):
    template_name="home/new_category.html"
    model = Category
    form_class = NewCategoryform
    success_url = reverse_lazy("home:index")


#Tarea---------------------------------------------------------------------------------------------------------------


class Crud(generic.View):
    tamplate_name="home/crud_tarea.html"
    context={}
    def get(self,request):
        return render(request, self.tamplate_name, self.context)
    





class Areas(generic.View):
    template_name="tarea/area/indexarea.html"
    context={}
    def get(self,request):
        self.context= {
            "areas": Area.objects.all()
        }
        return render(request, self.template_name, self.context)
    
class DetailAreas(generic.DetailView):
    template_name = "tarea/area/detail_areas.html"
    model = Area

class NewArea(generic.CreateView):
    template_name="tarea/area/new_area.html"
    model = Area
    form_class = NewAreaform
    success_url = reverse_lazy("home:areas")


class UpdateAreas(generic.UpdateView):
    template_name="tarea/area/update_areas.html"
    model = Area
    form_class = UpdateAreaForm
    success_url = reverse_lazy("home:areas")

class DeleteAreas(generic.DeleteView):
    template_name="tarea/area/delete_areas.html"
    model = Area
    success_url = reverse_lazy("home:areas")












class Pagina4(generic.View):
    tamplate_name="home/pagina4.html"
    context={}
    def get(self,request):
        return render(request, self.tamplate_name, self.context)

class Pagina5(generic.View):
    tamplate_name="home/pagina5.html"
    context={}
    def get(self,request):
        return render(request, self.tamplate_name, self.context)
    



class Pagina6(generic.View):
    tamplate_name="home/pagina6.html"
    context={}
    def get(self,request):
        return render(request, self.tamplate_name, self.context)

class Pagina7(generic.View):
    tamplate_name="home/pagina7.html"
    context={}
    def get(self,request):
        return render(request, self.tamplate_name, self.context)

class Pagina8(generic.View):
    tamplate_name="home/pagina8.html"
    context={}
    def get(self,request):
        return render(request, self.tamplate_name, self.context)

class Pagina9(generic.View):
    tamplate_name="home/pagina9.html"
    context={}
    def get(self,request):
        return render(request, self.tamplate_name, self.context)

class Pagina10(generic.View):
    tamplate_name="home/pagina10.html"
    context={}
    def get(self,request):
        return render(request, self.tamplate_name, self.context)






#Paginas_sin_Crud---------------------------------------------------------------------------------------




class Pagina11(generic.View):
    tamplate_name="home/pagina11.html"
    context={}
    def get(self,request):
        return render(request, self.tamplate_name, self.context)

class Pagina12(generic.View):
    tamplate_name="home/pagina12.html"
    context={}
    def get(self,request):
        return render(request, self.tamplate_name, self.context)

class Pagina13(generic.View):
    tamplate_name="home/pagina13.html"
    context={}
    def get(self,request):
        return render(request, self.tamplate_name, self.context)
    
class Pagina14(generic.View):
    tamplate_name="home/pagina14.html"
    context={}
    def get(self,request):
        return render(request, self.tamplate_name, self.context)

class Pagina15(generic.View):
    tamplate_name="home/pagina15.html"
    context={}
    def get(self,request):
        return render(request, self.tamplate_name, self.context)
    
class Pagina16(generic.View):
    tamplate_name="home/pagina16.html"
    context={}
    def get(self,request):
        return render(request, self.tamplate_name, self.context)

class Pagina17(generic.View):
    tamplate_name="home/pagina17.html"
    context={}
    def get(self,request):
        return render(request, self.tamplate_name, self.context)

class Pagina18(generic.View):
    tamplate_name="home/pagina18.html"
    context={}
    def get(self,request):
        return render(request, self.tamplate_name, self.context)

class Pagina19(generic.View):
    tamplate_name="home/pagina19.html"
    context={}
    def get(self,request):
        return render(request, self.tamplate_name, self.context)

class Pagina20(generic.View):
    tamplate_name="home/pagina20.html"
    context={}
    def get(self,request):
        return render(request, self.tamplate_name, self.context)