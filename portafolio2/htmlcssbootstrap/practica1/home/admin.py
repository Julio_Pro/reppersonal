from django.contrib import admin

from home import models

@admin.register(models.Profile)
class Profileadmin(admin.ModelAdmin):
    list_display=[
        "name", 
        "user", 
        "timestamp",
        "age",
        "status"
    ]

@admin.register(models.Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display=["name","user","id"]

@admin.register(models.Product)
class ProductAdmin(admin.ModelAdmin):
    list_display=["product_name","status","category","id"]

# Tarea------------------------------------

@admin.register(models.BankAccunt)
class BankAccuntAdmin(admin.ModelAdmin):
    list_display=["numbers_accunt", "name_bank"]

@admin.register(models.Worker)
class WorkerAdmin(admin.ModelAdmin):
    list_display=["name",
                  "bankaccunt",
                  "status"
                  ]

@admin.register(models.Area)
class AreaAdmin(admin.ModelAdmin):
    list_display=["name",
                  "description"
                  ]
    
@admin.register(models.Boss)
class BossAdmin(admin.ModelAdmin):
    list_display=["name",
                  "last_name",
                  "area"
                  ]

@admin.register(models.Creator)
class CreatorAdmin(admin.ModelAdmin):
    list_display=["name",
                  "status"
                  ]
@admin.register(models.Object)
class ObjectAdmin(admin.ModelAdmin):
    list_display=["name",
                  "description",
                  "creator"
                  ]

@admin.register(models.Factory)
class FactoryAdmin(admin.ModelAdmin):
    list_display=["name",
                  "address",
                  "state"
                  ]

@admin.register(models.Parts)
class PartsAdmin(admin.ModelAdmin):
    list_display=["name_part",
                  "disponibility",
                  "factory"
                  ]

@admin.register(models.Car)
class CarAdmin(admin.ModelAdmin):
    list_display=["name_car",
                  "car_registration"
                  ]

@admin.register(models.TAXI_DRIVER)
class TAXI_DRIVERdmin(admin.ModelAdmin):
    list_display=["name",
                  "size",
                  "car"
                  ]
