from django.shortcuts import render

from django.views import generic

from .models import Category, SubCategory, Brand, Product

# Create your views here.

class ListProduct(generic.View):
    template_name = "core/list_product.html"
    context = {}

    def get(self, request, *args, **kwargs):
        products = Product.objects.filter(status=True)
        self.context = {
            "products": products
        }
        return render(request, self.template_name, self.context)