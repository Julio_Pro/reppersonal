from home import views
from django.urls import path, include
app_name="home"
urlpatterns = [
    path('', views.index.as_view(), name="index"),
    path('about/', views.about.as_view(), name="about"),
    path('contact/', views.contact.as_view(), name="contact"),
]


