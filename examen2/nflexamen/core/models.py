from django.db import models

class Ciudades(models.Model):
    Nombre = models.CharField(max_length=35, default= "Minneapolis")
    Estado = models.CharField(max_length=15, default= "Minnesota")
    Descripcion = models.CharField(max_length=555, default= "Esta ciudad estadounidense es aficionada a los Minnesota")
    fecha= models.DateField(auto_now_add=True)
    def __str__(self):
        return self.Nombre

class Equipos(models.Model):
    Nombre = models.CharField(max_length=35, default= "Viking")
    Ciudad = models.ForeignKey(Ciudades, on_delete=models.CASCADE)
    Descripcion = models.CharField(max_length=555, default= "Este equipo se formo en 1990")
    fecha= models.DateField(auto_now_add=True)
    def __str__(self):
        return self.Nombre

class Estadios(models.Model):
    Nombre = models.CharField(max_length=35, default= "Bank Stadium")
    Equipos= models.ForeignKey(Equipos, on_delete=models.CASCADE)
    Descripcion = models.CharField(max_length=555, default= "El estadio abrio sus puertas en 2001")
    fecha= models.DateField(auto_now_add=True)
    def __str__(self):
        return self.Nombre