from django.urls import path
from core import views

app_name="core"
urlpatterns = [
    path('estadio/lista/',views.listaestadios.as_view(), name="listaestadio"),
    path('estadio/crear/',views.Createestadio.as_view(), name="crestadio"),
    path('estadio/detalles/<int:pk>/', views.Detailestadio.as_view(), name= "restadio"),
    path('estadio/actualizar/<int:pk>/', views.Updateestadio.as_view(), name= "uestido"),
    path('estadio/borrar/<int:pk>/', views.Deleteestadio.as_view(), name= "destadio"),
    
    path('equipo/lista/',views.Listaequipos.as_view(), name="listaequipo"),
    path('equipo/crear/',views.Createequipos.as_view(), name="crequipo"),
    path('equipo/detalles/<int:pk>/', views.Detailequipos.as_view(), name= "requipo"),
    path('equipo/actualizar/<int:pk>/', views.Updateequipos.as_view(), name= "uequipo"),
    path('equipo/borrar/<int:pk>/', views.Deleteequipos.as_view(), name= "dequipo"),

    path('ciudad/lista/',views.listaciudades.as_view(), name="listaciudad"),
    path('ciudad/crear/',views.Createcuidad.as_view(), name="crciudad"),
    path('ciudad/detalles/<int:pk>/', views.Detailciudad.as_view(), name= "rciudad"),
    path('ciudad/actualizar/<int:pk>/', views.Updateciudad.as_view(), name= "uciudad"),
    path('ciudad/borrar/<int:pk>/', views.Deleteciudad.as_view(), name= "dciudad"),
]